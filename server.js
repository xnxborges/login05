const express = require("express");
const conversorJson = require("body-parser");
const jwt = ("jsonwebtoken");
const SECRET = "nossoSegredo"

const app = express();

const porta = 3001

app.listen(porta, function (){
    console.log("Servidor rodando na porta " + porta)
})

app.use(conversorJson.urlencoded({extended: false}));
app.use(conversorJson.json());

function verificarUser(req, resp, next){
    const token = req.header("x-access-token")
    jwt.verify(token, SECRET, function(err, decoded){
        if(err){
            return resp.status(401).end()
        }
        req.dec = decoded.userId
        next()
    })
}

app.get("/", function (req,resp){
    resp.sendFile(__dirname + "/view/Login.html")
})
 
app.get("/usuario", verificarUser, function (req, resp){

    resp.json({msn: "Chegou na porta usuario - GET",
                user: req.query.nmUser,
                codigoUser: req.query.nmCodUser
            })
})

app.get("/usuario/:cod", function(req,resp){
    resp.json({msn: "Chegou na rota usuário - GET", codigo: req.params.cod})
})

app.post("/usuario", function(req, resp){
    resp.json({msn: "Chegou na porta do usuario - POST", user: req.body.nmUser, cod: req})
})

app.post("/login", function(req, resp){

    if (req.body.user == "XPTO" && req.body.pass == "1234") {
        //gerar token - payload
        const token = jwt.sign({userId: req.body.user}, SECRET, {expiresIn: 60})
        return resp.json({auth: true, token})
        
    }
    resp.status(402).end()
})